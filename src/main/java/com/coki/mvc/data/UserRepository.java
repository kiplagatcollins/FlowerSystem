package com.coki.mvc.data;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<Users, Integer> {
	public Users findByUsername(String username);

	

}
