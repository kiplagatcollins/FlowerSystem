<jsp:include page="/includes/header.jsp"></jsp:include>
 <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
    <div class="container">
   
    	<spring:url value="/flower/farmer/add" var="add"/>
            <form action="${add}" method="POST" enctype="multipart/form-data">
                <div class="w3-container text-center w3-text-blue"><h2>Add a Flower</h2></div>
		<div class="row">
	<div class="col-sm-3"></div>
			<div class="col-sm-6">

				<label class="w3-label">Name</label> 
				<input name="name"	class="w3-input" type="text"> <label class="w3-label">Quantity</label>
				<input name="quantity" class="w3-input" type="number"> 
				<label class="w3-label">Description</label>
				<textarea name="description" rows="4" cols="3" class="form-control"></textarea>

				<label class="w3-label">Image</label> <input name="file"
					class="form-control" type="file" width="25">
				<!--  <button class="btn btn-primary">Upload</button>-->
				<br> <label class="w3-label">Price</label> <input name="price" type="number" class="w3-input">

			</div>
			<div class="col-sm-3">

		</div>
		</div>

		<button class="btn btn-success w3-large w3-right">Save product</button>

                    </form>
     

        </div>
        <br><br>

<jsp:include page="/includes/footer.jsp"></jsp:include>