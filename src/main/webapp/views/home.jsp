<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<jsp:include page="/includes/header.jsp"></jsp:include>
<div class="container logopad">
    <div class="col-sm-2">

    </div>
    
    <div class="container">
           
        <div class="col-sm-1">
        </div>
        <div class="col-sm-10">

        
        <div class="row">
        
        <c:forEach items="${page.content}" var="flowers" >
            <div class="col-sm-3 logopad">
     
                <img src="data:image/jpg;base64,${flowers.base64image}" class="img" alt="${flowers.name }"/>
                <h1 class="">${flowers.name }</h1>
                <p>Price <span>${flowers.price }</span></p>
				<a class="w3-green w3-btn w3-middle text-center text-center" href='<spring:url value="/flower/customer/${flowers.flowerId}"/>'>Add to cart</a>
            </div>
            </c:forEach>
      
        </div>
         <div class="text-center">
            <ul class="pagination ">
                    <li><a href="#">&laquo;</a></li>
                    <li class="active"> <a href='<spring:url value="/?page=${page.number- 1 }"/>'>1</a></li>
                    <li ><a href="<spring:url value="/?page=${page.number+1}"/>">2</a></li>
                    <li><a href="<spring:url value="/?page=${page.number+2}"/>">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">&raquo;</a></li>
             </ul>
            </div>
            
         <a href='<spring:url value="/?page=${page.number }"/>'>Previous</a>
        <a href='<spring:url value="/?page=${page.number+ 1 }"/>'>Next</a>
       
    </div>
</div>
    </div>
   <jsp:include page="/includes/footer.jsp"></jsp:include>
   </body>
</html>